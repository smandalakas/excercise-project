package excercise.utils;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.WatchEvent.Kind;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exercise.annotations.Config;
import exercise.dataimporters.ImporterI;

@Stateless
public class FileSystemWatcherBean {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FileSystemWatcherBean.class);
	
	@Inject
	private ImporterI dataImporter;
	
	@Inject
	@Config("monitored_path")
	private String monitoredPath;
	
	public WatchService initializeWatcher(){
	
		WatchService watchService = null;
		try {
			watchService = FileSystems.getDefault().newWatchService();
		} catch (IOException e) {
			LOGGER.error("Failed to register event listening");
			return null;
		}
		Path path = Paths.get(monitoredPath);
		try {
			WatchKey watchKey = path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE,StandardWatchEventKinds.ENTRY_MODIFY);
		} catch (IOException e) {
			LOGGER.error("Failed to register event listening");
			return null;
		}
		return watchService;
	}
	
	public void poll(WatchService tocheck) throws InterruptedException {
		WatchKey key = tocheck.take();
		if (key != null) {
			for (WatchEvent<?> event : key.pollEvents()) {
				if(event.kind().toString().equalsIgnoreCase("ENTRY_CREATE") || event.kind().toString().equalsIgnoreCase("ENTRY_MODIFY")) {
					LOGGER.info("Event kind:" + event.kind() + ". File affected: " + event.context() + ".");
					Path path = (Path) event.context();
					dataImporter.importFile(path.toFile());
				}
				
				
			}
			key.reset();
		}
	}
	
	
}
