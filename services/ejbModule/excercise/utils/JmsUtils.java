package excercise.utils;

import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.Destination;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.TemporaryQueue;
import javax.jms.TextMessage;

@Stateless
public class JmsUtils {

	@Resource(mappedName = "java:/jms/queue/ExpiryQueue")
	private Queue serviceQueue;

	@Inject
	JMSContext context;

	private QueueBrowser serviceQueueBrowser;

	@PostConstruct
	public void init() {
		serviceQueueBrowser =	context.createBrowser(serviceQueue);	
	}
	
	// there's no need to explicitly call close here, since JMSContext implements
	// the Java SE 7 AutoCloseable Interface
	public void sendMessage(String txt) {
		sendMessage(txt,null, null);
	}

	public void sendMessage(String txt, Destination replyTo, String filename) {
		try {
			JMSProducer producer = context.createProducer();
			TextMessage message = context.createTextMessage(txt);
			if (replyTo != null) {
				message.setJMSReplyTo(replyTo);
			}
			if(filename != null) {
				message.setStringProperty("filename", filename);
			}
			producer.send(serviceQueue, message);
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}
	
	public String extractTextfromMessage(TextMessage msg) {
		try {
			return msg.getText();
		} catch (JMSException e) {
			System.out.println("Could not extract text from Message");
		}
		return null;
	}

	public Destination extractReplyToFromMessage(TextMessage msg) {
		try {
			return msg.getJMSReplyTo();
		} catch (JMSException e) {
			return null;
		}
	}
	
	public String extractPropertyFromMessage(TextMessage msg, String propertyKey) {
		try {
			return msg.getStringProperty(propertyKey);
		}catch( JMSException e) {
			return null;
		}
	}
	
}
