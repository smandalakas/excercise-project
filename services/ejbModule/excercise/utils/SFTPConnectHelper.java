package excercise.utils;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import exercise.annotations.Config;
import exercise.configuration.ConfigurationService;
import exercise.enums.configuration.StaticConfiguration;

@Stateless
public class SFTPConnectHelper {
	
	@Inject
	private ConfigurationService confService;
	
	@Inject
	@Config("sftp_download_path")
	private String ftpPth;
	
	
	//readme.txt
	private void connectToSFTP() throws JSchException, SftpException {
		//This code is sample: it actually 'hits' a Test public SFTP as this is the only way to test this code.
		//visit: http://test.rebex.net/
		
		JSch ssh = new JSch();
	    Session session = ssh.getSession("demo", "test.rebex.net", 22);
	    java.util.Properties config = new java.util.Properties();
	    config.put("StrictHostKeyChecking", "no");
	    session.setConfig(config);
	    session.setPassword("password");
	    
	    session.connect();
	    Channel channel = session.openChannel("sftp");
	    channel.connect();
	
	    ChannelSftp sftp = (ChannelSftp) channel;
	    sftp.get("readme.txt", ftpPth); //the file really exists on SFTP, but it does not allows for upload (even if it did, chances are that uploaded files will be deleted regularly).
	
	    sftp.disconnect();
	    channel.disconnect();
	    session.disconnect();
	
	}

	public void go() {
		try {
			connectToSFTP();
		} catch (JSchException e) {
			System.out.println("exception 1");
		} catch (SftpException e) {
			System.out.println("exception 2");
		}
	}
	
    
}
