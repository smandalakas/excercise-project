package excercise.utils;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public abstract class JpaUtils<E>{

	@PersistenceContext(unitName="persistenceUnit")
	protected EntityManager entityManager;
	
	protected final Class<E> entityClass;
	
	protected JpaUtils(Class<E> entityClass) {
		this.entityClass =entityClass;
	}
	
	public E persist(E entity) { return entityManager.merge(entity); }

	public void remove(E entity) { entityManager.remove(entity); }

	public E findById(Long id) { return entityManager.find(entityClass, id); }
	
	public <T> E findById(T id) { return entityManager.find(entityClass, id); }
	
	@SuppressWarnings("unchecked")
	public ArrayList<E> findAll() {
		ArrayList<E> allItems = new ArrayList<E>();
		String QueryText = "SELECT item FROM "+entityClass.getName()+" item";
		Query query =  entityManager.createQuery(QueryText);
		allItems.addAll((ArrayList<E>)query.getResultList());
		return allItems;
	}

}
