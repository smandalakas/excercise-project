package exercise.jms.services;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import excercise.utils.JmsUtils;
import exercise.transaction.services.TransactionService;
import exercise.transactionhandlerentities.UnsuccessfullTransactionProcess;

/**
 * Message-Driven Bean implementation class for: ErrorMessageConsumer
 */
@MessageDriven(
		activationConfig = { 
				@ActivationConfigProperty(
				propertyName = "destination", propertyValue = "java:/jms/queue/myErrorQueue"),
				@ActivationConfigProperty(
				propertyName = "destinationType", propertyValue = "javax.jms.Queue")
		}, 
		mappedName = "java:app/jms/myErrorQueue")
public class ErrorMessageConsumer implements MessageListener {

	@Inject
	private JmsUtils jmsUtils;
	
	@Inject
	private TransactionService service;
    /**
     * Default constructor. 
     */
    public ErrorMessageConsumer() {
        // TODO Auto-generated constructor stub
    }
	
	/**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message message) {
    	if(message instanceof TextMessage) {
    	TextMessage txt =(TextMessage)message;
    	String txtBody = jmsUtils.extractTextfromMessage(txt);
    	UnsuccessfullTransactionProcess failed = new UnsuccessfullTransactionProcess();
    	failed.setFileName(jmsUtils.extractPropertyFromMessage(txt, "filename"));
    	
    	if(txtBody.indexOf("::") != -1) {
	    	failed.setErrorMessage(txtBody.split("::")[0]);
	    	failed.setTransactionLine(txtBody.split("::")[1]);
    	}else {
    		failed.setErrorMessage(txtBody);
    	}
    	
    	service.storeTransactionError(failed);
    }
    	System.out.println(" **** Error message received  ****");   
    }
}
