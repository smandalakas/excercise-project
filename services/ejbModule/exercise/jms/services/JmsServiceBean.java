package exercise.jms.services;


import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;
import javax.jms.QueueBrowser;

import excercise.utils.JmsUtils;

import java.io.Serializable;

 
@Stateless
public class JmsServiceBean implements Serializable {

	private static final long serialVersionUID = -3492709269408539425L;
	
	@Inject
    JMSContext context;
	
	@Resource(lookup = "java:/jms/queue/myErrorQueue")
	private Queue errorQueue;
	
	
	@Inject
	private JmsUtils jmsUtils;
	
	private String filename = "";
	
	public void sendMessage(String line, String filename) {
		jmsUtils.sendMessage(line, errorQueue, filename);
	}
	

	/*
	 * Getters and Setters
	 */

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
	

	
}
