package exercise.jms.services;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import excercise.utils.JmsUtils;
import exercise.annotations.TransactionFactoryName;
import exercise.annotations.TransactionFactoryProducer;
import exercise.annotations.TransactionFactoryType;
import exercise.enums.transactionhandlers.TransactionKey;
import exercise.transaction.handlers.TransactionHandlerFactoryI;
import exercise.transaction.handlers.TransactionHandlerStrategy;

@MessageDriven(name = "ExpiryQueue", activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/ExpiryQueue"),
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge") })
public class MessageConsumer implements MessageListener {

	/*
	 * @Inject private AnnotatedTransactionHandlerFactory handlerFactory;
	 */

	@Inject
	@TransactionFactoryProducer
	@TransactionFactoryType(TransactionFactoryName.SIMPLE)
	private TransactionHandlerFactoryI factory;

	@Inject
	private JMSContext jmsContext;

	@Inject
	private JmsUtils utils;

	public void onMessage(Message message) {
		
		if (validateMessage(message)) {
			
			TextMessage msg = (TextMessage) message;
			Destination replyQueue = utils.extractReplyToFromMessage(msg);
			String text = utils.extractTextfromMessage(msg);
			String filename = utils.extractPropertyFromMessage(msg, "filename");
			
			if (text != null) {
				TransactionHandlerStrategy handler = factory
						.getApplicableHandler(TransactionKey.fromDescription(text.split(",")[9]));
				if (handler != null) {
					handler.handleTransaction(text);
				} else {
					if (replyQueue != null) {
						String errorTxt = ("could not find Strategy to handle message :: " + text);
						replyWithError(errorTxt, replyQueue, filename);
					}
				}
			} else {
				replyWithError("could not extract text from TextMessage ::" + text, replyQueue, filename);
			}
		}
	}

	private boolean validateMessage(Message msg) {
		if (msg instanceof TextMessage) {
			TextMessage txt = (TextMessage) msg;
			String text = utils.extractTextfromMessage(txt);
			return (text != null) && !text.trim().isEmpty() && (text.indexOf(",") != -1)
					&& (text.split(",").length <= 12);
		}
		return false;
	}

	private void replyWithError(String message, Destination jmsReplyTo, String filename) {
		if (jmsReplyTo == null || message == null) {
			return;
		}

		TextMessage msgToSend = jmsContext.createTextMessage();
		try {
			msgToSend.setText(message);
			if(filename != null && !filename.isEmpty()) {
				msgToSend.setStringProperty("filename", filename);
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}

		jmsContext.createProducer().send(jmsReplyTo, msgToSend);
	}

}