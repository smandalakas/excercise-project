package exercise.annotations;

import exercise.transaction.handlers.TransactionHandlerFactoryI;
import exercise.transaction.handlers.factories.AnnotatedTransactionHandlerFactory;
import exercise.transaction.handlers.factories.SimpleTransactionHandlerFactory;


public enum TransactionFactoryName {

    ANNOTATED (AnnotatedTransactionHandlerFactory.class),
    SIMPLE (SimpleTransactionHandlerFactory.class);
    

    private Class<? extends TransactionHandlerFactoryI> factoryType;

    private TransactionFactoryName(Class<? extends TransactionHandlerFactoryI> factoryType) {
        this.factoryType = factoryType;
    }

    public Class<? extends TransactionHandlerFactoryI> getFactoryType() {
        return factoryType;
    }
}