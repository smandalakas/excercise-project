package exercise.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import exercise.enums.transactionhandlers.*;
 
@Repeatable(TransactionHandlerForContainer.class)
@Retention(RUNTIME)
@Target({ TYPE, FIELD, METHOD, PARAMETER })
public @interface TansactionHandlerFor {
 TransactionKey type();
 String otherConstraint() default(""); //won't be used.. just for the demo
}
