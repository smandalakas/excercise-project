package exercise.webservices;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import exercise.websercices.restRequestDispatcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

@Stateless
public class restRequestDispatcherBean implements restRequestDispatcher{

	private static final Logger LOGGER = LoggerFactory.getLogger(restRequestDispatcherBean.class);

    private String errorMessages;

    @Override
    public String getStringDataFromURL(RequestType requestType, String requestURL) throws IOException {

        HttpURLConnection conn = createConnection(requestType, requestURL, false);
        if (conn.getResponseCode() != 200) {  
            LOGGER.error("Rest Webservice call {" + requestURL + "} Failed with HTTP error code : " + conn.getResponseCode());
            LOGGER.error("with error message : " + conn.getResponseMessage() != null && !conn.getResponseMessage().isEmpty() ?"Nothing received":conn.getResponseMessage() );
            logErrorMessages(conn);
            conn.disconnect();
            return "Connection Failed but could not get error message from connection!";
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), Charset.forName("UTF-8")));

        StringBuilder jsonString = new StringBuilder();
        String output;
        while ((output = br.readLine()) != null) {
            jsonString.append(output);
        }
        conn.disconnect();
        LOGGER.debug(jsonString.toString());
        return jsonString.toString();

    }

	@Override
    public void createSendJsonRequest(RequestType requestType, String requestURL, Object objectToSend, Class<?> clazz) throws IOException {
        errorMessages = null;
        System.out.println("requestURL : " + requestURL);
        ObjectWriter ow = new ObjectMapper().writerWithType(clazz);
        String json = ow.writeValueAsString(objectToSend);

     //   if (LOGGER.isDebugEnabled()) {
            LOGGER.info("Request Dispatcher will send object : " + json + " to URL : " + requestURL + " with Request Type set to : " + requestType.toString());  
     //   }
        
        HttpURLConnection conn = createConnection(requestType, requestURL, true);
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

        wr.write(json);
        wr.flush();
        wr.close();

        if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
            LOGGER.error("Rest Webservice call {" + requestURL + "} Failed with HTTP error code : " + conn.getResponseCode());
            LOGGER.error("with error message : " +  conn.getResponseMessage() != null && !conn.getResponseMessage().isEmpty()?"Nothing received":conn.getResponseMessage() );
            LOGGER.error("when Request Dispatcher sent object : " + json + " to URL : " + requestURL + " with Request Type set to : " + requestType.toString());

            String errorMessage = getErrorMessages(conn);
            logErrorMessages(conn);
            conn.disconnect();

            throw new IOException(errorMessage != null && !errorMessage.isEmpty()?"Failed to complete service provided by.." + requestURL:errorMessage);
        }
        conn.disconnect();
    }

    private HttpURLConnection createConnection(RequestType requestType, String requestURL, boolean willSendData) throws IOException {
        URL url = new URL(requestURL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(20000);
        conn.setRequestMethod(requestType.toString());
        conn.setRequestProperty("Accept", "application/json;charset=UTF-8");
        conn.setDoInput(true);
        if (willSendData) {
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
        }

        return conn;
    }

    private String getErrorMessages(HttpURLConnection connection) {
    	if(connection == null || connection.getErrorStream() == null ) {
    		return "Failed but could not get error message";
    	}
    	
        if (errorMessages == null) {
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getErrorStream()));

            StringBuilder errMsg = new StringBuilder();
            String input;
            try {
                while ((input = br.readLine()) != null) {
                    errMsg.append(input);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            errorMessages = errMsg.toString();
        }

        return errorMessages;
    }

    private void logErrorMessages(HttpURLConnection connection) {
        LOGGER.error("Error message from Service : " + getErrorMessages(connection));
    }
}
