package exercise.configuration;

import javax.ejb.Stateless;

import excercise.utils.JpaUtils;

@Stateless
public class PresentationConfigurationRepositoryBean  extends JpaUtils<PresentationConfiguration>{

	protected PresentationConfigurationRepositoryBean() {
		super(PresentationConfiguration.class);
	}

}
