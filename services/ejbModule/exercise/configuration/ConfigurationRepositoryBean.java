package exercise.configuration;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import excercise.utils.JpaUtils;
import exercise.configuration.Configuration;

@Stateless 
public class ConfigurationRepositoryBean  extends JpaUtils<Configuration> implements ConfigurationRepository{

	protected ConfigurationRepositoryBean() {
		super(Configuration.class);
	}


	public Configuration getConfigurationByCode(String code) {
		TypedQuery<Configuration> query =
				entityManager.createNamedQuery("Configuration.findByCode", Configuration.class).setParameter("code", code);
		
		List<Configuration> result = new ArrayList<>(query.getResultList());
		return (result != null && !result.isEmpty())?result.get(0):null; // i just don't like throwing an exception if no entity is found.
	}



	public List<Configuration> getAllConfigurations() {
		return findAll();
	}


	@Override
	public Configuration mergeConfiguration(Configuration selectedItem) {
		if(selectedItem.getCode() == null) {
			System.out.println("can't store null coded Configuration");
		}
		Configuration item = getConfigurationByCode(selectedItem.getCode());
		if(item == null) {
			return persist(selectedItem);
		}else {
			item.setValue(selectedItem.getValue());
			return persist(item);
		}
	}


	@Override
	public void removeConfiguration(Configuration config) {
		remove(findById(config.getId()));
	}

	
}
