package exercise.configuration;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

import exercise.annotations.Config;
import exercise.enums.configuration.StaticConfiguration;

@Stateless
public class ConfigurationServiceBean implements ConfigurationService {

	@Inject
	private ConfigurationRepository confRepo;
	
	@Override
	public String getConfigurationValue(StaticConfiguration item) {
		Configuration conf = confRepo.getConfigurationByCode(item.getCode());
		return (conf != null)?conf.getValue():item.getInitialValue();
	}

	@Override
	public String getConfigurationValueByCode(String code) {
		Configuration conf = confRepo.getConfigurationByCode(code);
		return (conf!=null)? conf.getValue():StaticConfiguration.getInitialValueFromCode(code);
	}
	
	@Produces
	@Config
	public String getConfiguration(InjectionPoint p) {
		Config config = p.getAnnotated().getAnnotation(Config.class);
		if(config.value() != null && !config.value().isEmpty()) {
		 return	getConfigurationValueByCode(config.value());
		}
		return null;
	}

}
