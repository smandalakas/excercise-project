package exercise.configuration;

import java.util.HashMap;

import javax.ejb.Stateless;
import javax.inject.Inject;

import exercise.common.Users;

@Stateless
public class PresentationConfigurationServiceBean implements PresentationConfigurationService {

	@Inject
	private PresentationConfigurationRepositoryBean presentationRepo;

	HashMap<String, Boolean> render = new HashMap<String, Boolean>();


	public void generateRenderMap(Users user) {
		for (PresentationConfiguration item : presentationRepo.findAll()) {
			if ((item.getAccessValue() & user.getUserRole().getRoleId()) > 0) {
				System.out.println("User has access for view : " + item.getViewId());
				render.put(item.getViewId(), true);
			} else {
				System.out.println("User DOES NOT HAVE access for view : " + item.getViewId());
				render.put(item.getViewId(), false);
			}
		}
	}

	/*
	 * Getters and Setters
	 */
	public HashMap<String, Boolean> getRender() {
		return render;
	}

	public void setRender(HashMap<String, Boolean> render) {
		this.render = render;
	}

}
