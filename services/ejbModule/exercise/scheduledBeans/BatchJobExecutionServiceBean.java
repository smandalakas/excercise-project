package exercise.scheduledBeans;

import java.io.IOException;
import java.nio.file.WatchService;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.jms.JMSDestinationDefinition;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import excercise.utils.FileSystemWatcherBean;

@JMSDestinationDefinition(
	    name = "java:/jms/queue/myErrorQueue",
	    interfaceName = "javax.jms.Queue",
	    description="myErrorQueue",
	    destinationName = "MyErrorQueue"
)
@Startup
@Singleton
/* @Clustered this is because of the assumption that resulting application will be used in clustered environment. 
			so, we don't want every single node to execute the business logic: {Wildfly} */
public class BatchJobExecutionServiceBean {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(BatchJobExecutionServiceBean.class);
	private WatchService watchService = null;
	
	@Inject
	private FileSystemWatcherBean fileWatcherService;
	
	
	@PostConstruct
	public void initialize() {
		LOGGER.info("Singleton sceduling bean is initializing.");
		watchService = fileWatcherService.initializeWatcher();
			
	}
	
	//timer goes off : working daysof week at 22:00 
	//connects to SFTP and gets file
	@Schedule(dayOfWeek="1-5", hour="22", persistent=true, info="Virtual SFTP call Timer")
	private void automaticTimeout() {
		LOGGER.info("Batch Service executing...");
	}
	
	// will actually trigger the mechanism for parsing csv file
	// obviously does not make sense to have it executed every 30 seconds .. but just for demo
	@Schedule(hour = "*", minute = "*/10", persistent = false, info="Check for new file in HDD Timer")
	private void fileWatcherPoller() {
		if(watchService != null) {
			try {
				fileWatcherService.poll(watchService);
			} catch (InterruptedException e) {
				LOGGER.error("FAILED to poll the File System Watcher Service for changes...");
			}
		}
		
	}
	
	@PreDestroy
	public void destroy() {
		try {
			watchService.close();
		} catch (IOException e) {
			LOGGER.error("Failed to Destroy File System Watch Service");
		}
	}
	
}
