package exercise.scheduledBeans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.Timer;
import javax.ejb.TimerService;

@Stateless
public class TimerInfoServiceBean implements TimerInfoService{

	@Resource
	TimerService timerService;
	 
	
	List<String> timerInfo = new ArrayList<>();
	
	
	
	public List<String> getTimersInfo() {
		timerInfo.clear();
		Collection<Timer> timers = timerService.getAllTimers();
		timers.stream().forEach(timer ->{ 
			StringBuilder sb = new StringBuilder();
			sb.append(timer.getInfo());
			sb.append("timer will go off at: ");
			sb.append(timer.getNextTimeout());
			timerInfo.add(sb.toString());
			
		});
		
		return timerInfo;
	}
}
