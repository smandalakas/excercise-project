package exercise.dataimporters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.concurrent.TimeUnit;


import javax.ejb.Stateless;
import javax.inject.Inject;

import excercise.utils.SFTPConnectHelper;
import exercise.jms.services.JmsServiceBean;

@Stateless
public class CvsFileImporter implements ImporterI {

	@Inject
	private SFTPConnectHelper sftpTools;
	
	@Inject
	private JmsServiceBean jmsService;
	 

	private static String importedCvFile = "/home/smandalakas/Desktop/temp/cvdData/MOCK_DATA.csv";
	
	public CvsFileImporter() { }
	
	public void importFile() {
		importFile(null);
	}

	@Override
	public void importFile(File fileToImport) {
		   long startTime = System.nanoTime();
		   long freeMemoryBefore = Runtime.getRuntime().freeMemory();
	
		   //Create file object
	    File file =  fileToImport==null?new File(importedCvFile):fileToImport;
	  
	    BufferedReader reader = null;
		try {
			//Use this one if we need other character encoding than the system it is running on and specify in InputStreamReader
			//reader = new BufferedReader(new InputStreamReader(new FileInputStream(importedCvFile))); 
			reader  = new BufferedReader(new FileReader(importedCvFile));
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	    
     reader.lines().forEach(line -> {

     	jmsService.sendMessage(line, file.getName());
         System.out.println(line);
     });
 
	    long endTime = System.nanoTime();
     long elapsedTimeInMillis = TimeUnit.MILLISECONDS.convert((endTime - startTime), TimeUnit.NANOSECONDS);
     System.out.println("Total elapsed time: " + elapsedTimeInMillis + " ms");
     long freeMemoryAfter = Runtime.getRuntime().freeMemory();
     
     System.out.println("free memory before : " + freeMemoryBefore + " - free memory after" + freeMemoryAfter);
     
     
     //Uncomment the line below to test the functionality of receving file from SFTP
     //as it is not called from anywhere
   //  sftpTools.go(); 
		
	}
	
	
}
