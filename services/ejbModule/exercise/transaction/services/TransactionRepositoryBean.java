package exercise.transaction.services;

import javax.ejb.Stateless;

import excercise.utils.JpaUtils;
import exercise.transactionhandlerentities.UnsuccessfullTransactionProcess;

@Stateless
public class TransactionRepositoryBean extends JpaUtils<UnsuccessfullTransactionProcess> {

	public TransactionRepositoryBean() {
		super(UnsuccessfullTransactionProcess.class);
	}

	
}
