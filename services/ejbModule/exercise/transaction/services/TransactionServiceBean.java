package exercise.transaction.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import exercise.transactionhandlerentities.UnsuccessfullTransactionProcess;

@Stateless
public class TransactionServiceBean implements TransactionService{

	@Inject
	TransactionRepositoryBean transactionsRepo;
	
	@Override
	public void storeTransactionError(UnsuccessfullTransactionProcess failedItem) {
		UnsuccessfullTransactionProcess item =	transactionsRepo.persist(failedItem);
	}

	@Override
	public List<UnsuccessfullTransactionProcess> getAllFailedItems() {
		return transactionsRepo.findAll();
	}

	@Override
	public UnsuccessfullTransactionProcess findFaildTransactionById(Long id) {
		return transactionsRepo.findById(id);
	}

}
