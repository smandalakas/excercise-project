package exercise.transaction.handlers;

import javax.enterprise.context.Dependent;

import exercise.annotations.TansactionHandlerFor;
import exercise.enums.transactionhandlers.TransactionKey;
import exercise.transaction.handlers.TransactionHandlerStrategy;


@Dependent
@TansactionHandlerFor(type = TransactionKey.SERV3)
public class SecondTransactionHandler implements TransactionHandlerStrategy{

	@Override
	public void handleTransaction(String transactionLine) {
		System.out.println("Second Transaction Handler is called... handling serv3");
	}

	@Override
	public boolean isApplicable(TransactionKey key) {
	 return	TransactionKey.SERV3.equals(key);
	}

}
