package exercise.transaction.handlers;

import javax.enterprise.context.Dependent;

import exercise.annotations.TansactionHandlerFor;
import exercise.enums.transactionhandlers.TransactionKey;
import exercise.transaction.handlers.TransactionHandlerStrategy;


@Dependent
@TansactionHandlerFor(type = TransactionKey.SERV1)
@TansactionHandlerFor(type = TransactionKey.SERV5)
public class FirstTransactionHandler implements TransactionHandlerStrategy{

	@Override
	public void handleTransaction(String transactionLine) {
		System.out.println("First Transaction Handler is called... handling serv1 and serv5");
	}

	@Override
	public boolean isApplicable(TransactionKey key) {
	 return	TransactionKey.SERV1.equals(key) || TransactionKey.SERV5.equals(key);
	}

}
