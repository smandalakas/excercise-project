package exercise.transaction.handlers.factories;

import javax.ejb.Stateless;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import exercise.annotations.TransactionFactoryName;
import exercise.annotations.TransactionFactoryType;
import exercise.enums.transactionhandlers.TransactionKey;
import exercise.transaction.handlers.TransactionHandlerFactoryI;
import exercise.transaction.handlers.TransactionHandlerStrategy;

@Dependent
public class SimpleTransactionHandlerFactory implements TransactionHandlerFactoryI{
	
	@Inject
	@Any
	private Instance<TransactionHandlerStrategy> handlers;
	
	public TransactionHandlerStrategy getApplicableHandler(TransactionKey key) {
		if(key != null) {
			for(TransactionHandlerStrategy strategy : handlers) {
				if(strategy.isApplicable(key)) {
					return strategy;
				}
			}
		}
		return null;
	}
}
