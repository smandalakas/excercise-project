package exercise.transaction.handlers.factories;

import java.util.Arrays;

import javax.ejb.Stateless;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import exercise.annotations.TansactionHandlerFor;
import exercise.annotations.TransactionFactoryName;
import exercise.annotations.TransactionFactoryType;
import exercise.annotations.TransactionHandlerForContainer;
import exercise.enums.transactionhandlers.TransactionKey;
import exercise.transaction.handlers.TransactionHandlerFactoryI;
import exercise.transaction.handlers.TransactionHandlerStrategy;
@Dependent
public class AnnotatedTransactionHandlerFactory implements TransactionHandlerFactoryI{

	@Inject
	@Any
	private Instance<TransactionHandlerStrategy> handlers;
	
	public TransactionHandlerStrategy getApplicableHandler(TransactionKey key) {
		if(key != null) {
			for(TransactionHandlerStrategy strategy : handlers) {
				AnnotationPopulation population = determineAnnotationPopulation(strategy);
				
				if(AnnotationPopulation.SINGLE.equals(population)) {
					TansactionHandlerFor hanlderFor = strategy.getClass().getAnnotation(TansactionHandlerFor.class);
					if(hanlderFor.type().equals(key)) {
						return strategy;
					}
				}
				
				else if(AnnotationPopulation.MULTI.equals(population)) {
					TransactionHandlerForContainer[] container = strategy.getClass().getAnnotationsByType(TransactionHandlerForContainer.class);
					for(TansactionHandlerFor handlerfor : Arrays.asList(container[0].value())) {
						if(handlerfor.type().equals(key)) {
							return strategy;
						}
					}
				
				}
			}
		}
		
		return null;
	}
	
	
	private AnnotationPopulation determineAnnotationPopulation(TransactionHandlerStrategy strategy) {
		if(strategy.getClass().isAnnotationPresent(TansactionHandlerFor.class)) {
			return AnnotationPopulation.SINGLE;
		}else if(strategy.getClass().isAnnotationPresent(TransactionHandlerForContainer.class)) {
			return AnnotationPopulation.MULTI;
		}
		
		return AnnotationPopulation.INVALID;
	}
	
	private enum AnnotationPopulation{
		SINGLE,
		MULTI,
		INVALID
	};
}
