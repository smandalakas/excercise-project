package exercise.transaction.handlers.factories;

import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.Annotated;
import javax.enterprise.inject.spi.InjectionPoint;

import exercise.annotations.TransactionFactoryProducer;
import exercise.annotations.TransactionFactoryType;
import exercise.transaction.handlers.TransactionHandlerFactoryI;

@Stateless
public class TransactionHandlerFacoryProducer {
	
	@Produces
	@TransactionFactoryProducer
	public TransactionHandlerFactoryI getStrategyFactory(@Any Instance<TransactionHandlerFactoryI> instance, InjectionPoint injectionPoint) {
		 Annotated annotated = injectionPoint.getAnnotated();
		 TransactionFactoryType factoryTypeAnnotation = annotated.getAnnotation(TransactionFactoryType.class);
	     Class<? extends TransactionHandlerFactoryI> factoryType = factoryTypeAnnotation.value().getFactoryType();
	     return instance.select(factoryType).get();
	}
}
