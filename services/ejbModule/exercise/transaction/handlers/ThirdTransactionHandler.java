package exercise.transaction.handlers;

import javax.enterprise.context.Dependent;

import exercise.annotations.TansactionHandlerFor;
import exercise.enums.transactionhandlers.TransactionKey;
import exercise.transaction.handlers.TransactionHandlerStrategy;


@Dependent
@TansactionHandlerFor(type = TransactionKey.SERV2)
@TansactionHandlerFor(type = TransactionKey.SERV4)
public class ThirdTransactionHandler implements TransactionHandlerStrategy{

	@Override
	public void handleTransaction(String transactionLine) {
		System.out.println("Third Transaction Handler is called... handling serv2 and serv4");
	}

	@Override
	public boolean isApplicable(TransactionKey key) {
	 return	TransactionKey.SERV2.equals(key) || TransactionKey.SERV4.equals(key);
	}

}
