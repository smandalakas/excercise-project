package exercise.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({
	@NamedQuery(name = "findUserByUserName", query = "SELECT c FROM Users c where c.userName = :userName"),
	@NamedQuery(name = "validateUserNamePassword", query = "Select c from Users c where c.userName = :userName and c.password = :password") 
})
@Entity
@Table(name = "exercise_users")
public class Users {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int userId;

	@Column(nullable = false)
	private String firstName;

	@Column(nullable = false)
	private String LastName;

	@Column(unique = true, nullable = false)
	private String userName;

	@Column(nullable = false)
	private String password;

	@Column(name = "user_account_active")
	private Boolean accountActive;

	@ManyToOne
	@JoinColumn(name = "roleId")
	private Roles userRole;

	public Users() {

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((LastName == null) ? 0 : LastName.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + userId;
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Users other = (Users) obj;
		if (userId != other.getUserId()) {
			return false;
		}
		// if (LastName == null) {
		// if (other.LastName != null)
		// return false;
		// } else if (!LastName.equals(other.LastName))
		// return false;
		// if (firstName == null) {
		// if (other.firstName != null)
		// return false;
		// } else if (!firstName.equals(other.firstName))
		// return false;
		// if (userId != other.userId)
		// return false;
		// if (userName == null) {
		// if (other.userName != null)
		// return false;
		// } else if (!userName.equals(other.userName))
		// return false;
		return true;
	}

	/*
	 * Getters and Setters
	 */
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Roles getUserRole() {
		return userRole;
	}

	public void setUserRole(Roles userRole) {
		if (userRole != null) {
			System.out.println("userRole set to : " + userRole.getRoleDescription());
		}
		this.userRole = userRole;
	}

	public Boolean getAccountActive() {
		return accountActive;
	}

	public void setAccountActive(Boolean accountActive) {
		this.accountActive = accountActive;
	}

}
