package exercise.configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="presentation_configuration")
public class PresentationConfiguration {

	@Id
	@Column(name="view_id",nullable=false, unique=true)
	private String viewId;
	
	@Column(name="access_value")
	private int accessValue;

	/*
	 * Getters and Setters
	 */
	public int getAccessValue() {
		return accessValue;
	}

	public void setAccessValue(int accessValue) {
		this.accessValue = accessValue;
	}

	public String getViewId() {
		return viewId;
	}

	public void setViewId(String viewId) {
		this.viewId = viewId;
	}
}
