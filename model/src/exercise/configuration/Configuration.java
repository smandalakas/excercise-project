package exercise.configuration;

import java.io.Serializable;
import java.lang.Long;
import java.lang.String;
import javax.persistence.*;

@Entity
@Cacheable
@Table(name="exercise_configuration")
@NamedQueries({
    @NamedQuery(name="Configuration.findByCode",
                query="SELECT c FROM Configuration c where c.code = :code")
}) 
public class Configuration implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Configuration(String code, String value) {
		this.code = code;
		this.value = value;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(name="config_code", unique=true, nullable=false)
	private String code;
	
	@Column(name="config_value",nullable=false)
	private String value;
	

	public Configuration() {
		super();
	}   
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}   
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}
   
}
