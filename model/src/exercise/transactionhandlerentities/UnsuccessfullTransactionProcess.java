package exercise.transactionhandlerentities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: UnsuccessfullTransactionProcess
 *
 */
@Entity
@Table(name="failed_to_process_transaction")
public class UnsuccessfullTransactionProcess implements Serializable {

	private static final long serialVersionUID = 1L;

	public UnsuccessfullTransactionProcess() {
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name="file_name")
	private String fileName;
	
	@Column(name="error_message")
	private String errorMessage;
	
	@Column(name="transaction_line_received")
	private String transactionLine;

	/*
	 * Getters and Setters
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getTransactionLine() {
		return transactionLine;
	}

	public void setTransactionLine(String transactionLine) {
		this.transactionLine = transactionLine;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
