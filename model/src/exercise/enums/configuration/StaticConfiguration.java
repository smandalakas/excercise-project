package exercise.enums.configuration;

public enum StaticConfiguration {
	SFTP_DOWNLOAD_PATH("sftp_download_path","/home/smandalakas/Development/Projects/watchedFolder"),
	WEB_SERVICE_URL("web_service_url","http://www.mocky.io/v2/5aa555de2e0000860da4044e"),
	MONITORED_PATH("monitored_path","/home/smandalakas/Development/Projects/watchedFolder")
	
;
	private final String code;
	private final String initialValue;
	
	public String getCode() {
		return code;
	}
	
	public String getInitialValue() {
		return initialValue;
	}
	
	private StaticConfiguration(String code, String initValue) {
		this.code = code;
		this.initialValue = initValue;
	}
	
	public static String getInitialValueFromCode(String code) {
		if(code == null) {return null;}
		
		for(StaticConfiguration item : values()) {
			if(item.code.equalsIgnoreCase(code)) {
				return item.initialValue;
			}
		}
		return null;
	}
}
