package exercise.enums.transactionhandlers;

public enum TransactionKey {
	SERV1("SERV1"),
	SERV2("SERV2"),
	SERV3("SERV3"),
	SERV4("SERV4"),
	SERV5("SERV5");

	private String description;
	
	private TransactionKey(String description){
		this.description = description;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public static TransactionKey fromDescription(String description) {
		if(description == null) {return null;}
		
		for(TransactionKey item : values()) {
			if(item.description.equalsIgnoreCase(description)) {
				return item;
			}
		}
		
		return null;
	}
}
