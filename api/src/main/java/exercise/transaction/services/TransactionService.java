package exercise.transaction.services;

import java.util.List;

import exercise.transactionhandlerentities.UnsuccessfullTransactionProcess;

public interface TransactionService {

	void storeTransactionError(UnsuccessfullTransactionProcess failedItem);
	
	//This is for DEMO only!!! it makes sense because of H2, and only for one time run!
	List<UnsuccessfullTransactionProcess> getAllFailedItems();
	UnsuccessfullTransactionProcess findFaildTransactionById(Long id);
}
