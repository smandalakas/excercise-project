package exercise.transaction.handlers;

import exercise.enums.transactionhandlers.TransactionKey;

public interface TransactionHandlerFactoryI {
	TransactionHandlerStrategy getApplicableHandler(TransactionKey key); 
}
