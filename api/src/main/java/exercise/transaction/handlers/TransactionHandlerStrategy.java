
package exercise.transaction.handlers;

import exercise.enums.transactionhandlers.*;

public interface TransactionHandlerStrategy {
	
	void handleTransaction(String transactionLine);
	boolean isApplicable(TransactionKey Key);
}
