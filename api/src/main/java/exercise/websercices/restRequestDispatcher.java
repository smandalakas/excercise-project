package exercise.websercices;

import java.io.IOException;

public interface restRequestDispatcher {
		enum RequestType { GET, POST, DELETE ,PUT }
		 
		String getStringDataFromURL(RequestType requestType, String requestURL) throws IOException;
		void createSendJsonRequest(RequestType requestType, String requestURL, Object objectToSend, Class<?> clazz) throws IOException;
	}
	