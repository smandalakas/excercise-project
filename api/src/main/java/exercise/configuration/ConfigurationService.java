package exercise.configuration;

import javax.enterprise.inject.spi.InjectionPoint;

import exercise.enums.configuration.StaticConfiguration;

public interface ConfigurationService {
	String getConfigurationValue(StaticConfiguration item);
	String getConfigurationValueByCode(String code);
	String getConfiguration(InjectionPoint p);
}
