package exercise.configuration;

import java.util.List;

import exercise.configuration.Configuration;

public interface ConfigurationRepository {

  Configuration	getConfigurationByCode(String code);
  List<Configuration> getAllConfigurations();
  Configuration mergeConfiguration(Configuration selectedItem);
void removeConfiguration(Configuration config);
}
