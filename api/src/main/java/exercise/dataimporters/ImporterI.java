package exercise.dataimporters;

import java.io.File;

public interface ImporterI {

	void importFile();

	void importFile(File file);
}
