package exercise.scheduledBeans;

import java.util.List;


public interface TimerInfoService {
	public List<String> getTimersInfo();
}
