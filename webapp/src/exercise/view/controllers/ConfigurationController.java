package exercise.view.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import exercise.configuration.Configuration;
import exercise.configuration.ConfigurationRepository;
import exercise.enums.configuration.StaticConfiguration;


@ManagedBean
@ViewScoped
public class ConfigurationController implements Serializable {
	
	@Inject
	private ConfigurationRepository confRepo;
	
	private List<Configuration> allConfigurations = new ArrayList<>();
	private List<StaticConfiguration> allStaticConf = new ArrayList<>();
	
	private Configuration selectedItem = new Configuration();
	private StaticConfiguration itemToOverwrite;
	
	
	@PostConstruct
	public void init() {
	//	if(facesContext.isPostback()) {
	//		return ;
	//	}
		allConfigurations.addAll(confRepo.getAllConfigurations());
		allStaticConf.addAll(Arrays.asList(StaticConfiguration.values()));
	}
	
	public boolean checkIfOverriden(StaticConfiguration itemToCheck) {
		return	(findLocalItemByCode(itemToCheck.getCode()) != null)?true:false;
	}
	
	public void prepereForOverride(StaticConfiguration itemToOverride) {
		this.itemToOverwrite = itemToOverride;
		selectedItem = new Configuration();
		selectedItem.setCode(itemToOverride.getCode());
	}
	
	public void saveSelectedItem() {
		selectedItem.setCode(itemToOverwrite.getCode());
		selectedItem = confRepo.mergeConfiguration(selectedItem);
		Configuration existingItem = findLocalItemByCode(selectedItem.getCode());
		if(existingItem != null) {
			allConfigurations.remove(existingItem);
		}
		allConfigurations.add(selectedItem);
	}
	
	private Configuration findLocalItemByCode(String code) {
		return allConfigurations.stream()
				.filter(conf -> conf.getCode().equalsIgnoreCase(code))
				.findAny()
				.orElse(null);
	}
	
	public void deleteConfiguration(Configuration config) {
			confRepo.removeConfiguration(config);
			allConfigurations.remove(config);
	}
	/*
	 * Getters and Setters
	 */
	public List<Configuration> getAllConfigurations() {
		return allConfigurations;
	}
	
	public List<StaticConfiguration> getAllStaticConfigurations() {
		return allStaticConf;
	}

	public Configuration getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(Configuration selectedItem) {
		this.selectedItem = selectedItem;
	}

}

