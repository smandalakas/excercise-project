package exercise.view.controllers;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import exercise.annotations.Config;
import exercise.dataimporters.ImporterI;
import exercise.scheduledBeans.TimerInfoService;
import exercise.transaction.services.TransactionService;
import exercise.transactionhandlerentities.UnsuccessfullTransactionProcess;
import exercise.websercices.restRequestDispatcher;
import exercise.websercices.restRequestDispatcher.RequestType;

@ManagedBean
@RequestScoped
public class MainController {

	@Inject
	private ImporterI importService;
	

	@Inject
	private TransactionService service;
	
	@Inject
	private TimerInfoService timerInfoService;
	
	@Inject
	private restRequestDispatcher requestDisp;
	
	@Inject
	@Config(value="web_service_url")
	private String service_url;
	
	private List<UnsuccessfullTransactionProcess> allFailed = new ArrayList<>();
	private List<String> timerInfo;
	
	@PostConstruct
	public void init() {
		refreshTimerInfo();
	}

	public MainController() {	}
	
	public void handleFile() {
		importService.importFile();
	}
	
	public void rehreshFailed() {
		allFailed.clear();
		allFailed.addAll(service.getAllFailedItems());
		System.out.println(allFailed.size());
	}
	
	public void refreshTimerInfo() {
		setTimerInfo(timerInfoService.getTimersInfo());
	}
	
	public void checkRequestModule() {
	String messageReceived = null;
		try {
			messageReceived = requestDisp.getStringDataFromURL(RequestType.GET, service_url);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(messageReceived != null) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Successful",  "Your message: " + messageReceived) );
		}
	}
	
	
	/*
	 * Getters and Setters
	 */
	public List<UnsuccessfullTransactionProcess> getAllFailed() {
		return allFailed;
	}

	public void setAllFailed(List<UnsuccessfullTransactionProcess> allFailed) {
		this.allFailed = allFailed;
	}

	public List<String> getTimerInfo() {
		return timerInfo;
	}

	public void setTimerInfo(List<String> timerInfo) {
		this.timerInfo = timerInfo;
	}

	
	

}
